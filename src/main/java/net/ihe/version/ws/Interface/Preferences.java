package net.ihe.version.ws.Interface;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Preferences {
    private static Logger log = LoggerFactory.getLogger(Preferences.class);
    private static Properties preferences;
    private static Object preferencesSync = new Object();

    private Preferences() {
        super();
    }

    public static String getProperty(String key) {
        return getPreferences().getProperty(key);
    }

    private static Properties getPreferences() {

        preferences = new Properties();
        InputStream preferencesStream = Preferences.class.getResourceAsStream("/gzl.version.properties");
        try {
            preferences.load(preferencesStream);
        } catch (IOException e) {
            log.error(e.getStackTrace().toString());
        }
        return preferences;
    }
}
