package net.ihe.version.ws.Interface;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Local
@Path("/version")
public interface VersionProviderInterface {

	@GET
	@Path("/")
	@Produces("text/xml")
	public String getVersion();
}
