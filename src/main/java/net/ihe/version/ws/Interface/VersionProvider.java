package net.ihe.version.ws.Interface;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Stateless(name = "VersionProvider")
public class VersionProvider implements VersionProviderInterface {

	@GET
	@Path("/")
	@Produces("application/xml")
	public String getVersion() {
		String version = get();
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<version>" + version + "</version>";
	}

	public static String get() {
		return Preferences.getProperty("buildVersion");
	}
}